﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace wwitz_api.Models
{
    public class EmployeeProvider
    {
        private string connectionString;
        public EmployeeProvider() {
            connectionString = "";
        }
        
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public IEnumerable<Employee> GetAll(string whereClause)
        {
            using (IDbConnection dbConnection = Connection)
            {

                string sQuery = @"select [Employee_Id]
                            ,[Full_Name]
                            ,[Position_List_Order]
                            ,[Position_Name]
                            ,[Location]
                            ,[Qualification]
                            ,[Bio]
                            ,[Ask_Me_About]
                            ,[Mobile_Num]
                            ,[username]
                            ,[1_Business_Unit] as Business_Unit
                            ,[1_Business_Unit_Style] as Business_Unit_Style
                            ,[2_Service_Area] as Service_Area
                            ,[2_Service_Area_Style] as Service_Area_Style
                            ,[3_Operating_Unit] as Operating_Unit
                            ,[3_Operating_Unit_Style] as Operating_Unit_Style
                            ,[FLAG]
                            FROM DEVELOPMENT.WWITZ_V2 " + whereClause
                            + "ORDER BY Position_List_Order, Full_Name ASC";
                dbConnection.Open();
                return dbConnection.Query<Employee>(sQuery);
            }
        }
    }

}
