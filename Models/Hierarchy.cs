﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wwitz_api.Models
{
    public class Hierarchy
    {
        public string Location_Name { get; }
        public string Business_Unit { get; }
        public string Service_Area { get; }
        public string Operating_Unit { get; }
        public int Active { get; }

    }
}
