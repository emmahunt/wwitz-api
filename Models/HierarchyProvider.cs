﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace wwitz_api.Models
{
    public class HierarchyProvider
    {
        private string connectionString;
        public HierarchyProvider() {
            connectionString = "";
        }
        
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public IEnumerable<Hierarchy> GetAll()
        {
            using (IDbConnection dbConnection = Connection)
            {

                string sQuery = @"SELECT [Location_Name]
                                ,[Business_Unit]
                                ,[Service_Area]
                                ,[Operating_Unit_Clean] as [Operating_Unit]
                                ,[Active]
                            FROM [DT_WWITZ_2019].[DEVELOPMENT].[HIERARCHY];";
                dbConnection.Open();
                return dbConnection.Query<Hierarchy>(sQuery);
            }
        }
    }

}
