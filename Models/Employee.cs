﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wwitz_api.Models
{
    public class Employee
    {
        public int Employee_Id { get; }
        public string Full_Name { get; }
        public int Position_List_Order { get; }
        public string Position_Name { get; }
        public string Location { get; }
        public string Qualification { get; }
        public string Bio { get; }
        public string Ask_Me_About { get; }
        public string Mobile_Num { get; }
        public string username { get; }
        public string Business_Unit { get; }
        public string Business_Unit_Style { get; }
        public string Service_Area { get; }
        public string Service_Area_Style { get; }
        public string Operating_Unit { get; }
        public string Operating_Unit_Style { get; }
        public string FLAG { get; }

    }
}
