﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using wwitz_api.Models;

namespace wwitz_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAccessPolicy")]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeProvider employeeProvider;

        public EmployeeController()
        {
            employeeProvider = new EmployeeProvider();
        }
        [HttpGet("{location}/{businessUnit}/{serviceArea}/{operatingUnit}/{skill}")]

        public Tuple<Dictionary<string,List<Employee>>, int>  Get(string location, string businessUnit, string serviceArea, string operatingUnit, string skill)
        {
            // Only split skills into Array, as we don't know how many will be selected, and need to generate a WHERE clause for each skill in the selected Array. The other params are strings in the format of a comma separated list, so can be converted into a SQL IN statement without conversion to an Array first 
            String [] skillArray = skill.Split(",".ToCharArray());

            String whereClause = " where username is not null";

            // Create a dictionary to make it easier to loop through params and generate the necessary where clauses
            Dictionary<string, string> filterDict = new Dictionary<string, string>();

            filterDict.Add("[Location]", location);
            filterDict.Add("[1_Business_Unit]", businessUnit);
            filterDict.Add("[2_Service_Area]", serviceArea);
            filterDict.Add("[3_Operating_Unit]", operatingUnit);

            foreach (var key in filterDict.Keys)
            {
                if(filterDict[key] != "all"){
                    // Generate the where clause by converting the param (a comma separated list string) into a SQL IN statement by wrapping each list item in quotes 
                    whereClause = whereClause + " and " + key + " in ('" + filterDict[key].Replace(",", "','") + "')";
                }

                // If the parameter is 'all', we don't need to apply a where clause for that field
                else{
                    whereClause = whereClause + "";
                }
            };

            if(skillArray[0] != "No skill filters"){
                foreach(string skillFilter in skillArray){
                
                whereClause = whereClause + " and [Ask_Me_About] like '%" + skillFilter + "%'";
                }
            }
                
        
        
            var employees = employeeProvider.GetAll(whereClause);
            var employeeGroups = new Dictionary<string, List<Employee>>();

            var count = employees.Count();

            // Create results of the right format (with Role titles as keys) for the front end
            foreach (Employee employee in employees)
                {
                    if(employeeGroups.ContainsKey(employee.Position_Name)){
                        employeeGroups[employee.Position_Name].Add(employee);
                    }
                    else{
                        employeeGroups.Add(employee.Position_Name,new List<Employee>{employee});
                    }
                }
            var returnTuple = Tuple.Create(employeeGroups, count);
           

            return returnTuple;
        }
    }
}