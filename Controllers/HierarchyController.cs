﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using wwitz_api.Models;

namespace wwitz_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAccessPolicy")]
    public class HierarchyController : ControllerBase
    {
        private readonly HierarchyProvider hierarchyProvider;

        public HierarchyController()
        {
            hierarchyProvider = new HierarchyProvider();
        }

        [HttpGet("hierarchy")]
        public Dictionary<string, Dictionary<string, Dictionary<string, IEnumerable<string>>>>  Get()
        {
        
            var hierarchyResults = hierarchyProvider.GetAll();

            var locations = new List<string>();
            var businessUnits = new List<string>();
            var serviceAreas = new List<string>();
            var operatingUnits = new List<string>();

            var hierarchy = hierarchyResults
                .GroupBy(gp => gp.Location_Name)
                .ToDictionary(
                    k => k.Key,
                    k => k.GroupBy(gp => gp.Business_Unit)
                        .ToDictionary(
                            k => k.Key,
                            k => k.GroupBy(gp => gp.Service_Area)
                            .ToDictionary(
                                k => k.Key,
                                k => k.Select(x => x.Operating_Unit)
                            )
                        )
                );

            foreach(Hierarchy row in hierarchyResults){
                if(!locations.Contains(row.Location_Name)){
                    locations.Add(row.Location_Name);
                }
                if(!businessUnits.Contains(row.Business_Unit)){
                    businessUnits.Add(row.Business_Unit);
                }
                if(!serviceAreas.Contains(row.Service_Area)){
                    serviceAreas.Add(row.Service_Area);
                }
                if(!operatingUnits.Contains(row.Operating_Unit)){
                    operatingUnits.Add(row.Operating_Unit);
                }
            };

            hierarchy.Add("allValues", new Dictionary<string, Dictionary<string, IEnumerable<string>>>());
            hierarchy["allValues"].Add("allValues",new Dictionary<string, IEnumerable<string>>());
            hierarchy["allValues"]["allValues"].Add("allLocations",locations);
            hierarchy["allValues"]["allValues"].Add("allBusinessUnits",businessUnits);
            hierarchy["allValues"]["allValues"].Add("allServiceAreas",serviceAreas);
            hierarchy["allValues"]["allValues"].Add("allOperatingUnits",operatingUnits);

            return hierarchy;
        }
    }
}

